from fastapi.testclient import TestClient
from src.main import app

client = TestClient(app)


def test_two():
    res = client.get('/')
    assert res.status_code == 200
    assert res.json() == {"status": 200, "message": "test"}


# def test_one():
#     res = client.post('/export/productsales', json={"YEAR":1996, "MONTH1":8, "MONTH2": 9})
#     assert res.status_code == 200
    # assert res.json()=={"status": 200, "message": "export data success"}
