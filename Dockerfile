FROM python:3.9.18

COPY ./requirements.txt requirements.txt

RUN pip3 install --upgrade pip
RUN pip3 install -r ./requirements.txt

COPY ./src /src

COPY ./tests /tests

CMD ["uvicorn", "src.main:app", "--host", "0.0.0.0", "--port", "8888"]

EXPOSE 8888


# File "/usr/local/lib/python3.9/smtplib.py", line 652, in auth
#     authobject(challenge).encode('ascii'), eol='')