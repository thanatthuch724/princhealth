
# Prince-health test : Ci/Cd pipeline to Client host.

## CI/CD pipeline Overview

![Alt text](images/image.png)


## Data ingestion Overview
![Alt text](images/image-1.png)

## S1.Productsalesamount_ingestion
![Alt text](images/image-2.png)![Alt text](images/image-3.png)
- example API : 
- `curl -X POST -H "Content-Type: application/json" -d '{"filename":"1996.xlsx", "specified_month": "Sep"}' http://0.0.0.0:8888/ingest/productsales`
- `curl -X POST -H "Content-Type: application/json" -d '{"filename":"1996.xlsx", "quater": 4}' http://0.0.0.0:8888/ingest/quater/productsales`


## S2.Productsalesamount_Rerun

![Alt text](images/image-5.png)

- Rerun : - `curl -X POST -H "Content-Type: application/json" -d '{"filename":"1996.xlsx", "specified_month": "Sep"}' http://0.0.0.0:8888/rerun/productsales`

## S2.ProductSalesamount_export

![Alt text](images/image-6.png)

example API: 
- quaterly: `curl -X POST -H "Content-Type: application/json" -d '{"year":1996, "quater":3}' http://0.0.0.0:8888/export/productsales/quater`
- monthly: `curl -X POST -H "Content-Type: application/json" -d '{"year":1996, "month":"Aug"}' http://0.0.0.0:8888/export/productsales/month`

## S3.SupplierShipduration

![Alt text](images/image-7.png)![Alt text](images/image-8.png)

example API: `curl -X POST -H "Content-Type: application/json" -d '{"filename":"SupplierShipDuration.xlsx", "specified_date": "1997-01-01"}' http://0.0.0.0:8888/ingest/shipduration`

## S4.Customer

![Alt text](images/image-9.png)

## S4.Customer Logic

![Alt text](images/image-10.png) ![Alt text](images/image-11.png)

example API: `curl -X GET http://0.0.0.0:8888/ingest/customer`

## TIPS: Step Create Jenkins pipeline

 - go to Dashboard > New item > Enter an item name > Select Pipeline > OK
 - Enable Github project > Git repository
 - Pipeline (Definition): Generate jenkinspipeline script


## TIPS: Email notification Jenkins

- Jenkins browser 'Dash board' > 'Manage Jenkins' > 'System'
![Alt text](images/image222.png)


![Alt text](images/image-111.png)

* Password - From gmail `App password`

# Presentation End Thank you.