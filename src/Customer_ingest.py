import os
import psycopg2
import pandas
import random
import string
from dotenv import load_dotenv

"""
 ------- PROCESS INGEST DATA OF `Customer`
"""

load_dotenv()


CRT_TABLE : str = "crt.Customer";
RAW_TABLE : str = "raw.Customer";
SRC_TABLE : str = "raw.Customers";

DB_HOST : str = os.getenv("DB_HOST");
DB_PORT : str = os.getenv("DB_PORT");
DB_NAME : str = os.getenv("DB_NAME");
DB_USER : str = os.getenv("DB_USER");
DB_PASS : str = os.getenv("DB_PASS");
EX_PATH : str = os.getenv("EX_PATH");
SRC_CUSTOMER_FILE : str = os.getenv("SRC_CUSTOMER_FILE");


db_params : dict = {
    "host": DB_HOST,
    "port": DB_PORT,
    "database": DB_NAME,
    "user": DB_USER,
    "password": DB_PASS,
};

UPDATE_LOGIC = f"""UPDATE crt.Customer B
SET
    companyname = A.companyname,
    contactnamge = A.contactnamge,
    contacttitle = A.contacttitle,
    address = A.address,
    region = A.region,
    postalcode = A.postalcode,
    country = A.country,
    phone = A.phone,
    fax = A.fax
FROM raw.Customer A
WHERE
    B.customerID = A.customerID
    AND (
        B.customerID IS NOT NULL
        OR B.companyname <> A.companyname
        OR B.contactnamge <> A.contactnamge
        OR B.contacttitle <> A.contacttitle
        OR B.address <> A.address
        OR B.region <> A.region
        OR B.postalcode <> A.postalcode
        OR B.country <> A.country
        OR B.phone <> A.phone
        OR B.fax <> A.fax
);
""";

INSERT_LOGIC = f"""INSERT INTO {CRT_TABLE}
SELECT 
A.customerid,
A.companyname,
A.contactnamge,
A.contacttitle,
A.address,
A.city,
A.region,
A.postalcode,
A.country,
A.phone,
A.fax
FROM {RAW_TABLE} A
LEFT JOIN {CRT_TABLE} B
ON A.customerID = B.CustomerID
WHERE B.customerID is NULL;"""



def encrypt(i):
    #CLEAN 
    if i == None:
        temp = None
    else:
        s = ""
        for k in i:
            if k.isdigit() or k == ".":
                s+= k
        #ENCRYPT
        temp = ""
        for j in s:
            random_text = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(3))
            temp+=random_text + chr(ord(j)-10)
    return temp


def decrypt(text):
    temp = ""
    result = text[3::4]
    for r in result:
        temp += chr(ord(r)+10)
    return temp

def Load_data_to_csv_temp():
    Connection = psycopg2.connect(**db_params);
    SQL = f"SELECT * FROM {SRC_TABLE};"
    dataFrame  = pandas.read_sql(SQL, Connection)
    dataFrame['phone'] = dataFrame['phone'].apply(encrypt)
    dataFrame['fax'] = dataFrame['fax'].apply(encrypt)
    dataFrame.to_csv(SRC_CUSTOMER_FILE, index=False)
    pass

def Truncate_raw_Customer():
    Connection = psycopg2.connect(**db_params);
    SQL = f"TRUNCATE {RAW_TABLE};";
    curosr = Connection.cursor();
    curosr.execute(SQL)
    Connection.commit()

# Load `DataFrame` into `RAW_TABLE`
def Load_dataframe_to_raw(raw_table: str=RAW_TABLE) -> None:
    Connection = psycopg2.connect(**db_params);
    cursor = Connection.cursor()
    try:
        with open(SRC_CUSTOMER_FILE, "r") as data_file:
            cursor.copy_expert(f"COPY {RAW_TABLE} FROM stdin WITH CSV HEADER", data_file)
            Connection.commit()
        return {"status": "success"}
    except :
        return {"status": "faill"}


# UPDATE DATA
def update_data_crt_table():
    Connection = psycopg2.connect(**db_params);
    curosr = Connection.cursor();
    curosr.execute(UPDATE_LOGIC)
    Connection.commit()
    Connection.close()


# INSERT DATA
def insert_data_crt_table():
    Connection = psycopg2.connect(**db_params);
    curosr = Connection.cursor();
    curosr.execute(INSERT_LOGIC)
    Connection.commit()
    Connection.close()


