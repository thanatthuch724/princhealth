
# *Correctness 
# - Validation and Testing
# - Error Handling
    
# *High maintainaility
# - Modular Design
# - Documentation
    
# *High Readability
# - Code Standards and Styles
# - Meaningful Naming and Comments
    

import os
import psycopg2
import pandas
from dotenv import load_dotenv

"""
 ------- PROCESS INGEST DATA OF `ProductSalesAmountByMonth`
"""



############################################################### DECLARE

load_dotenv()



RAW_TABLE : str = "raw.ProductSalesAmountByMonth";
CRT_TABLE : str = "crt.ProductSalesAmountByMonth";
DB_HOST : str = os.getenv("DB_HOST");
DB_PORT : str = os.getenv("DB_PORT");
DB_NAME : str = os.getenv("DB_NAME");
DB_USER : str = os.getenv("DB_USER");
DB_PASS : str = os.getenv("DB_PASS");
SR_PATH : str = os.getenv("SR_PATH");
TMP_FILE: str = os.getenv("TMP_FILE")

############################################################### DATABASE CONNECTION 
db_params : dict = {
    "host": DB_HOST,
    "port": DB_PORT,
    "database": DB_NAME,
    "user": DB_USER,
    "password": DB_PASS,
};

SQL_LOGIC : str = f"""
INSERT INTO {CRT_TABLE}        
SELECT A.yearMonth, A.ProductID, A.ProductName, A.salesAmount,
CASE WHEN A.yearMonth > B.yearMonth THEN ((A.salesAmount / B.salesAmount)-1) *100 ELSE NULL END percentage_change
FROM {RAW_TABLE} A
LEFT JOIN (
	SELECT t1.*
	FROM {CRT_TABLE} t1
	JOIN (
		SELECT ProductID, MAX(DATE(yearMonth || '-01')) AS max_start_date
		FROM {CRT_TABLE}
		GROUP BY ProductID 
	) t2
	ON t1.ProductID = t2.ProductID
	AND DATE(t1.yearMonth || '-01') = t2.max_start_date) B
ON A.ProductID = B.ProductID
WHERE NOT EXISTS (
	SELECT 1
	FROM {CRT_TABLE} C
	WHERE C.ProductID = A.ProductID
	AND C.yearMonth = A.yearMonth
);
"""





############################################################### PROCESS INGEST DATA OF `ProductSalesAmountByMonth`

"""
----------------------------------------------------------
Load data of new month > RAW_TABLE > Transform > CRT_TABLE
----------------------------------------------------------
""" 

# Load data from `xlsx` as `DataFrame`
def Load_excel_as_dataframe(file_name: str, quater: [int,str]) -> pandas.DataFrame:

    monthlist : list = [];
    # Convert Quater number into `Month string` to prepare for Sheet names
    if quater == 1:
        monthlist = ['Jan', 'Feb', 'Mar'];
    elif quater == 2:
        monthlist = ['Apr', 'May', 'Jun'];
    elif quater == 3:
        monthlist = ['Jul', 'Aug', 'Sep'];
    elif quater == 4:
        monthlist = ['Oct', 'Nov', 'Dec'];

    file_directory :str = f"{SR_PATH}/{file_name}";
    empty_df = pandas.DataFrame();

    try:
        try:
            # LOAD data for each months.
            for sheet_name in monthlist:
                dataFrame = pandas.read_excel(file_directory, sheet_name=sheet_name);
                dataFrame = dataFrame[["yearMonth", "ProductID", "ProductName", "salesAmount"]];
                empty_df  = pandas.concat([empty_df, dataFrame], ignore_index=True);
        except:
            pass
        # WRITE DATA Source to tempfile.
        empty_df.to_csv(TMP_FILE, index=False);
        return {"status": "success"}
    except :
        return {"status": "faill"} 

# Truncate data from `RAW_TABLE`
def Truncate_raw_ProductSalesAmount(raw_table: str = RAW_TABLE):
    SQL : str = f"TRUNCATE {raw_table};";
    Connection = psycopg2.connect(**db_params);
    
    curosr = Connection.cursor()
    try:
        curosr.execute(SQL)
        Connection.commit()
        return {"status": "success"}
    except :
        return {"status": "faill"}


# Load `DataFrame` into `RAW_TABLE`
def Load_dataframe_to_raw(raw_table: str=RAW_TABLE) -> None:
    Connection = psycopg2.connect(**db_params);
    cursor = Connection.cursor()
    try:
        with open(TMP_FILE, "r") as data_file:
            cursor.copy_expert(f"COPY {raw_table} FROM stdin WITH CSV HEADER", data_file)
            Connection.commit()
        return {"status": "success"}
    except :
        return {"status": "faill"}
    

# Transform and Load data logic to `CRT ZONE`
def Transform_and_Load(sql_logic: str=SQL_LOGIC):
    Connection = psycopg2.connect(**db_params);
    cursor = Connection.cursor()
    try:
        cursor.execute(sql_logic)
        Connection.commit()
        return {"status": "success"}
    except :
        return {"status": "faill"}






