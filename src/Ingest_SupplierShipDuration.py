
import os
import psycopg2
import pandas
from dotenv import load_dotenv

"""
 ------- PROCESS INGEST DATA OF `SupplierShipduration`
 STEP
 1. Load latest data of SupplierShipduration from excel to RAW_TABLE
 2. TRUNCATE CRT_TABLE THEN insert.

# --INSERT INTO crt.suppliershipduration
# --SELECT A.supplierid, A.shipcountry, AVG(EXTRACT(DAY FROM B.ShippedDate - B.OrderDate)) AS "duration(day)", CAST(TO_CHAR(CURRENT_TIMESTAMP, 'YYYY-MM-DD HH24:MI:SS') AS timestamp) was_calculated_to, CAST(TO_CHAR(CURRENT_TIMESTAMP, 'YYYY-MM-DD HH24:MI:SS') AS timestamp) last_updated_at
# --FROM raw.suppliershipduration A
# --JOIN crt.Orders B
# --ON A.shipcountry = B.shipcountry
# --GROUP BY A.supplierid, A.shipcountry;
"""



load_dotenv()


RAW_TABLE : str = "raw.suppliershipduration";
CRT_TABLE : str = "crt.suppliershipduration";
CRT_ORDER : str = "crt.Orders"
specified_date : str = ""



DB_HOST : str = os.getenv("DB_HOST");
DB_PORT : str = os.getenv("DB_PORT");
DB_NAME : str = os.getenv("DB_NAME");
DB_USER : str = os.getenv("DB_USER");
DB_PASS : str = os.getenv("DB_PASS");
SR_PATH : str = os.getenv("SR_PATH");
ShipDurationTemp: str = os.getenv("ShipDurationTemp");

db_params : dict = {
    "host": DB_HOST,
    "port": DB_PORT,
    "database": DB_NAME,
    "user": DB_USER,
    "password": DB_PASS,
};

def Tranfrom_File_to_CSV(ShipDurationFile: str):
    file_directory = "./src/Shipduration"+ "/" +ShipDurationFile
    dataFrame = pandas.read_excel(file_directory)
    dataFrame = dataFrame[["SupplierID", "ShipCountry"]]
    dataFrame.to_csv(ShipDurationTemp, index=False)

def Truncate_RAW():
    Connection = psycopg2.connect(**db_params)
    Cursor = Connection.cursor()
    SQL = f"Truncate table {RAW_TABLE};"
    Cursor.execute(SQL)
    Connection.commit()
    Connection.close()

def Load_CSV_to_RAW():
    Connection = psycopg2.connect(**db_params);
    Cusor = Connection.cursor()
    with open(ShipDurationTemp, "r") as data_file:
        Cusor.copy_expert(f"COPY {RAW_TABLE} FROM stdin WITH CSV HEADER", data_file)
        Connection.commit()
        Connection.close()

def truncate_target():
    Connection = psycopg2.connect(**db_params);
    Cursor = Connection.cursor()   
    SQL = f"Truncate table {CRT_TABLE};"
    Cursor.execute(SQL)
    Connection.commit()
    Connection.close()

def Transform_and_Load(specified_date):
    Connection = psycopg2.connect(**db_params);
    Cusor = Connection.cursor()
    SQL_LOGIC = f"""INSERT INTO crt.suppliershipduration
    SELECT A.supplierid, A.shipcountry, 
    AVG(EXTRACT(DAY FROM B.ShippedDate - B.OrderDate)) AS "duration(day)",
    '{specified_date} 00:00:00' was_calculated_to,
    CAST(TO_CHAR(CURRENT_TIMESTAMP, 'YYYY-MM-DD HH24:MI:SS') AS timestamp) last_updated_at
    FROM {RAW_TABLE} A
    JOIN {CRT_ORDER} B
    ON A.shipcountry = B.shipcountry
    WHERE B.orderdate<='{specified_date}'
    GROUP BY A.supplierid, A.shipcountry;"""
    Cusor.execute(SQL_LOGIC)
    Connection.commit()
    Connection.close()




