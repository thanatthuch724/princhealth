from typing import Union, Optional
from pydantic import BaseModel
from fastapi import FastAPI, status
from src.notification import sendEmail

app = FastAPI()

RecipientEmail = "thanatthuchkamseng@gmail.com";

class ItemIngest(BaseModel):
    filename: str
    specified_month: str = None

class ItemIngestQuater(BaseModel):
    filename: str
    quater: Optional[Union[int, str]] = None

class monthQuery(BaseModel):
    year: Optional[Union[int, str]]
    month: Optional[Union[int, str]]

class qurterQuery(BaseModel):
    year: Optional[Union[int, str]]
    quater: Optional[Union[int, str]]

class ItemShip(BaseModel):
    filename: str
    specified_date: str




################################################################################ 
################################ Senario 1 #####################################
################################################################################
    

@app.post("/ingest/productsales", status_code=status.HTTP_200_OK)
async def ingest_data(body: ItemIngest):
    from src.Ingest_ProductSalesAmount_monthly import Truncate_raw_ProductSalesAmount, Load_excel_as_dataframe, Load_dataframe_to_raw, Transform_and_Load
    try:
        Truncate_raw_ProductSalesAmount()
        Load_excel_as_dataframe(file_name=body.filename, specified_month=body.specified_month)
        Load_dataframe_to_raw()
        Transform_and_Load()

        # EMAIL NOTIFICATION
        message = f"The `ProductSalesAmount Ingestion {body.filename}` {body.specified_month} pipeline has SUCCEEDED. \nBest Regard,\nThanatthuch Kamseng"
        sendEmail("Pipeline Notification",message, RecipientEmail)
        return {"status": 200, "Message": "Ingestion Success."}
    except :
        message = f"The `ProductSalesAmount Ingestion {body.filename}` {body.specified_month}  pipeline has FAILED. \nBest Regard,\nThanatthuch Kamseng"
        sendEmail(
            "Pipeline Notification",
            message,
            RecipientEmail
        )
        return {
            "status": 400,
            "Message": "Ingestion Failed."
        };



@app.post("/ingest/quater/productsales", status_code=status.HTTP_200_OK)
async def ingest_data_quater(body: ItemIngestQuater):
    from src.Ingest_ProductSalesAmount_quaterly import Truncate_raw_ProductSalesAmount, Load_excel_as_dataframe, Load_dataframe_to_raw, Transform_and_Load
    try:
        Truncate_raw_ProductSalesAmount()
        Load_excel_as_dataframe(file_name=body.filename, quater=body.quater)
        Load_dataframe_to_raw()
        Transform_and_Load()

        # EMAIL NOTIFICATION
        message = f"The `ProductSalesAmount Ingestion {body.filename}` Quater {body.quater} pipeline has SUCCEEDED. \nBest Regard,\nThanatthuch Kamseng"
        sendEmail("Pipeline Notification",message, "thanatthuchkamseng@gmail.com")
        return {"status": 200,"Message": "Ingestion Success."}
    except :
        message = f"The `ProductSalesAmount Ingestion {body.filename}` Quater {body.quater} pipeline has FAILED. \nBest Regard,\nThanatthuch Kamseng"
        sendEmail("Pipeline Notification",message, RecipientEmail)
        return {
            "status": 400,
            "Message": "Ingestion Failed."
        };




@app.post("/rerun/productsales", status_code=status.HTTP_200_OK)
async def ingest_data(body: ItemIngest):
    from src.Rerun_ProductSalesAmount import Truncate_raw_ProductSalesAmount, Load_excel_as_dataframe, Load_dataframe_to_raw, Transform_and_Load, delete_newer_data
    try:
        Truncate_raw_ProductSalesAmount()
        Load_excel_as_dataframe(file_name=body.filename)
        Load_dataframe_to_raw()
        delete_newer_data()
        Transform_and_Load()

        # EMAIL NOTIFICATION
        message = f"The `ProductSalesAmount RERUN {body.filename}` pipeline has SUCCEEDED. \nBest Regard,\nThanatthuch Kamseng"
        sendEmail("Pipeline Notification",message, RecipientEmail)
        return {
            "status": 200,
            "Message": "RERUN Success."
        }
    except :
        message = f"The `ProductSalesAmount RERUN {body.filename}` pipeline has FAILED. \nBest Regard,\nThanatthuch Kamseng"
        sendEmail("Pipeline Notification",message, RecipientEmail)
        return {
            "status": 200,
            "Message": "RERUN Failed."
        };



################################################################################ 
################################ Senario 2 #####################################
################################################################################


@app.post("/export/productsales/month", status_code=status.HTTP_200_OK)
async def exportdata_month(body: monthQuery):
    from src.Export_ProductSalesAmount_monthly import export_data
    try:
        export_data(body.year, body.month)
        return {
            "status": 200, 
            "message": "export data success"
        }
    except:
        return {
            "status": 400,
            "message": "Export data erorr"
        };



@app.post("/export/productsales/quater", status_code=status.HTTP_200_OK)
async def exportdata_quater(body: qurterQuery):
    from src.Export_ProductSalesAmount_quater import export_data
    try:
        export_data(body.year, body.quater)
        return {
            "status": 200,
            "message": "export data success"
        }
    except:
        return {
            "status": 400,
            "message": "Export data erorr"
        };


################################################################################ 
################################ Senario 3 #####################################
################################################################################


@app.post("/ingest/shipduration", status_code=status.HTTP_200_OK)
async def ingest_ship(body: ItemShip):
    try:
        from src.Ingest_SupplierShipDuration import Tranfrom_File_to_CSV, Truncate_RAW, Load_CSV_to_RAW,truncate_target, Transform_and_Load
        Tranfrom_File_to_CSV(body.filename)
        Truncate_RAW()
        Load_CSV_to_RAW()
        truncate_target()
        Transform_and_Load(body.specified_date)

        # EMAIL NOTIFICATION
        message = f"The `ShipDuration INGEST {body.filename}` pipeline has SUCCEEDED. \nBest Regard,\nThanatthuch Kamseng"
        sendEmail("Pipeline Notification",message, "thanatthuchkamseng@gmail.com")
        return {"status": 200, "Message": "Ingest Success."}
    except :
        message = f"The `ShipDuration INGEST {body.filename}` pipeline has FAILED. \nBest Regard,\nThanatthuch Kamseng"
        sendEmail("Pipeline Notification",message, "thanatthuchkamseng@gmail.com")
        return {
            "status": 200, 
            "Message": "Ingest Failed."
        };



################################################################################ 
################################ Senario 4######################################
################################################################################

@app.get("/ingest/customer")
async def ingest_customer():
    try:
        from src.Customer_ingest import Load_data_to_csv_temp, Truncate_raw_Customer, Load_dataframe_to_raw, update_data_crt_table, insert_data_crt_table
        Load_data_to_csv_temp()
        Truncate_raw_Customer()
        Load_dataframe_to_raw()
        update_data_crt_table()
        insert_data_crt_table()

        # EMAIL NOTIFICATION
        message = f"The `Customer INGEST` pipeline has SUCCEEDED. \nBest Regard,\nThanatthuch Kamseng"
        sendEmail("Pipeline Notification",message, RecipientEmail)
        return {"status": 200, "Message": "Ingest Success."}
    except :
        message = f"The `Customer INGEST` pipeline has FAILED. \nBest Regard,\nThanatthuch Kamseng"
        sendEmail("Pipeline Notification",message, RecipientEmail)
        return {"status": 200, "Message": "Ingest Failed."}



@app.get("/")
async def test_function():
    return {"status": 200, "message": "test"}




@app.get("/test")
async def test_testfunction():
    return {"status": 200, "message": "test"}
