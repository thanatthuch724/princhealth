
import os
import json
import pandas
import psycopg2
import requests
from dotenv import load_dotenv
import requests



"""
 ------- PROCESS export DATA OF `ProductSalesAmountByMonth`
"""

load_dotenv()


CRT_TABLE : str = "crt.ProductSalesAmountByMonth";
DB_HOST : str = os.getenv("DB_HOST");
DB_PORT : str = os.getenv("DB_PORT");
DB_NAME : str = os.getenv("DB_NAME");
DB_USER : str = os.getenv("DB_USER");
DB_PASS : str = os.getenv("DB_PASS");
EX_PATH : str = os.getenv("EX_PATH")
# ACCESS_TOKEN : str = os.getenv("ACCESS_TOKEN")
GoogleAPIExchangeToken : str = os.getenv("GoogleAPIExchangeToken")

res = requests.post(GoogleAPIExchangeToken)

ACCESS_TOKEN = res.json()['access_token']


db_params : dict = {
    "host": DB_HOST,
    "port": DB_PORT,
    "database": DB_NAME,
    "user": DB_USER,
    "password": DB_PASS,
};



def convert_month(month):

    month_dict = {
        '01': 'Jan',
        '02': 'Feb',
        '03': 'Mar',
        '04': 'Apr',
        '05': 'May',
        '06': 'Jun',
        '07': 'Jul',
        '08': 'Aug',
        '09': 'Sep',
        '10': 'Oct',
        '11': 'Nov',
        '12': 'Dec'
    }
    year, month_number = month.split('-')
    return f"{month_dict[month_number]} {year}";

#EXPORT DATA
def export_data(year: int = None, quater: int = None):
    Connection = psycopg2.connect(**db_params);

    MONTH1 = None
    MONTH2 = None

    # Declare Range of Quaterly
    if quater == 1:
        MONTH1 = '01'
        MONTH2 = '03'
    elif quater == 2:
        MONTH1 = '04'
        MONTH2 = '06'
    elif quater == 3:
        MONTH1 = '07'
        MONTH2 = '09'
    else:
        MONTH1 = '10'
        MONTH2 = '12'
    
    # Prepare file name to export.
    file_name : str = str(f"{year}{MONTH1}-{MONTH2}").replace("01","Jan")\
        .replace("02","Feb")\
            .replace("03","Mar")\
                .replace("04","Apr")\
                    .replace("05","May")\
                        .replace("06","Jun")\
                            .replace("07","Jul")\
                                .replace("08","Aug")\
                                    .replace("09","Sep")\
                                        .replace("10","Oct")\
                                            .replace("11","Nov")\
                                                .replace("12","Dec");
    

    SQL_LOGIC_QUERY : str = f"""SELECT * FROM {CRT_TABLE} WHERE yearMonth BETWEEN '{year}-{MONTH1}' and '{year}-{MONTH2}'; """

   
    df = pandas.read_sql(SQL_LOGIC_QUERY, Connection)  # Storing data from DB to DataFrame.
    
    excel_writer = pandas.ExcelWriter(f'{EX_PATH}/{file_name}.xlsx', engine='xlsxwriter') #Write excel engine.

    # Convert month numbers to Month string for Excel Sheet name.
    for yearmonth, group in df.groupby('yearmonth'):
        yearmonth = convert_month(yearmonth)
        yearmonth=yearmonth.split(" ")[0]
        group.to_excel(excel_writer, sheet_name=yearmonth, index=False)
    excel_writer._save()


    # EXPORT to SHARE DRIVE

    headers = {"Authorization": f"Bearer {ACCESS_TOKEN}"}
    param = {
        "name": f'{file_name}.xlsx',
        "parents": ["15qiJnE2QMsutOtzP6QEYOc6UbxhbE2Hj"]
    }

    files = {
        'data': ('metadata', json.dumps(param), 'application/json; charset=UTF-8'),
        'file': (f'{EX_PATH}/{file_name}.xlsx', open(f'{EX_PATH}/{file_name}.xlsx', "rb"))
    }

    r = requests.post(
        url="https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart",
        headers=headers,
        files=files
    )

    return df, file_name



