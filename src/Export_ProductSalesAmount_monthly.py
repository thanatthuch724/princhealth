
import os
import psycopg2
import pandas
import requests
from dotenv import load_dotenv

"""
 ------- PROCESS INGEST DATA OF `ProductSalesAmountByMonth`
"""

load_dotenv()


CRT_TABLE : str = "crt.ProductSalesAmountByMonth";

DB_HOST : str = os.getenv("DB_HOST");
DB_PORT : str = os.getenv("DB_PORT");
DB_NAME : str = os.getenv("DB_NAME");
DB_USER : str = os.getenv("DB_USER");
DB_PASS : str = os.getenv("DB_PASS");
EX_PATH : str = os.getenv("EX_PATH")
# ACCESS_TOKEN : str = os.getenv("ACCESS_TOKEN")
GoogleAPIExchangeToken : str = os.getenv("GoogleAPIExchangeToken")

res = requests.post(GoogleAPIExchangeToken)

ACCESS_TOKEN = res.json()['access_token']

db_params : dict = {
    "host": DB_HOST,
    "port": DB_PORT,
    "database": DB_NAME,
    "user": DB_USER,
    "password": DB_PASS,
};


def convert_month(month):

    month_dict = {
        '01': 'Jan',
        '02': 'Feb',
        '03': 'Mar',
        '04': 'Apr',
        '05': 'May',
        '06': 'Jun',
        '07': 'Jul',
        '08': 'Aug',
        '09': 'Sep',
        '10': 'Oct',
        '11': 'Nov',
        '12': 'Dec'
    }
    year, month_number = month.split('-')
    return f"{month_dict[month_number]} {year}";


def month_to_number(month):
    month_dict = {
        'Jan': '01',
        'Feb': '02',
        'Mar': '03',
        'Apr': '04',
        'May': '05',
        'Jun': '06',
        'Jul': '07',
        'Aug': '08',
        'Sep': '09',
        'Oct': '10',
        'Nov': '11',
        'Dec': '12'
    }
    return month_dict.get(month, 'Unknown')

#EXPORT DATA
def export_data(year: int = None, month_string: int = None):
    month_number = month_to_number(month_string)
    Connection = psycopg2.connect(**db_params);
    cursor = Connection.cursor()
    SQL_LOGIC_MONTH1: str = f"""SELECT * FROM {CRT_TABLE} WHERE yearMonth='{year}-{month_number}';"""
    
    file_name : str = ""
    df = pandas.read_sql(SQL_LOGIC_MONTH1, Connection)
    file_name = str(year)+str(month_string).replace("1","Jan").replace("2","Feb").replace("3","Mar").replace("4","Apr").replace("5","May").replace("6","Jun").replace("7","Jul").replace("8","Aug").replace("9","Sep").replace("10","Oct").replace("11","Nov").replace("12","Dec")

    excel_writer = pandas.ExcelWriter(f'{EX_PATH}/{file_name}.xlsx', engine='xlsxwriter')
    for yearmonth, group in df.groupby('yearmonth'):
        yearmonth = convert_month(yearmonth)
        yearmonth=yearmonth.split(" ")[0]
        group.to_excel(excel_writer, sheet_name=yearmonth, index=False)
    excel_writer._save()


    # EXPORT to SHARE DRIVE
    import json
    import requests


    headers = {"Authorization": f"Bearer {ACCESS_TOKEN}"}
    param = {
        "name": f'{file_name}.xlsx',
        "parents": ["15qiJnE2QMsutOtzP6QEYOc6UbxhbE2Hj"]
    }

    files = {
        'data': ('metadata', json.dumps(param), 'application/json; charset=UTF-8'),
        'file': (f'{EX_PATH}/{file_name}.xlsx', open(f'{EX_PATH}/{file_name}.xlsx', "rb"))
    }


    r = requests.post(
        url="https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart",
        headers=headers,
        files=files
    )


    return df, file_name



# # EXPORT FILE



# # file_name = "test"
# # EXPORT_PATH = "/Users/spy/Documents/working_space/jenkins/unit_test/src/test_export"
    
# # excel_writer = pandas.ExcelWriter(f'{EXPORT_PATH}/{file_name}.xlsx', engine='xlsxwriter')

# df = export_data(1996,8)


